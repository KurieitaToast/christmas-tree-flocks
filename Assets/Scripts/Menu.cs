﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private Configuration config;
    [SerializeField] private Configuration defaultConfig;

    [SerializeField] private TMP_InputField maxVelocityField;
    [SerializeField] private TMP_InputField minStartVelocityField;
    [SerializeField] private TMP_InputField maxStartVelocityField;
    [SerializeField] private TMP_InputField steeringSpeedField;
    [SerializeField] private TMP_InputField neighborRadiusField;
    [SerializeField] private TMP_InputField boidCountField;
    [SerializeField] private TMP_InputField targetFollowRadiusField;
    [SerializeField] private TMP_InputField seperationRadiusField;
    [SerializeField] private TMP_InputField cohesionWeightField;
    [SerializeField] private TMP_InputField alignmentWieghtField;
    [SerializeField] private TMP_InputField seperationWeightField;
    [SerializeField] private TMP_InputField targetWieghtField;
    [SerializeField] private TMP_InputField fleeWeightField;
    [SerializeField] private TMP_InputField fleeRadiusField;
    [SerializeField] private TMP_InputField cameraSpeedField;
    [SerializeField] private TMP_InputField cameraRotationSpeedField;
    [SerializeField] private TMP_InputField enemyCountField;
    [SerializeField] private TMP_InputField targetCountField;
    [SerializeField] private TMP_InputField enemyMaxVelocityField;
    [SerializeField] private TMP_InputField targetMaxVelocityField;
    [SerializeField] private Toggle trailsEnabledField;
    [SerializeField] private Toggle targetMovementEnabledField;
    [SerializeField] private Toggle enemyMovementEnabledField;

    public void Awake()
    {
        ResetToDefault();
    }

    public void Changed()
    {
        config.maxVelocity = float.Parse(maxVelocityField.text);
        config.minStartVelocity = float.Parse(minStartVelocityField.text);
        config.maxStartVelocity = float.Parse(maxStartVelocityField.text);
        config.steeringSpeed = float.Parse(steeringSpeedField.text);
        config.neighborRadius = float.Parse(neighborRadiusField.text);
        config.targetFollowRadius = float.Parse(targetFollowRadiusField.text);
        config.seperationRadius = float.Parse(seperationRadiusField.text);
        config.fleeRadius = float.Parse(fleeRadiusField.text);
        config.weights[0] = float.Parse(cohesionWeightField.text);
        config.weights[1] = float.Parse(alignmentWieghtField.text);
        config.weights[2] = float.Parse(seperationWeightField.text);
        config.weights[3] = float.Parse(targetWieghtField.text);
        config.weights[4] = float.Parse(fleeWeightField.text);
        config.cameraSpeed = float.Parse(cameraSpeedField.text);
        config.rotationSpeed = float.Parse(cameraRotationSpeedField.text);
        config.trailsEnabled = trailsEnabledField.isOn;
        config.targetMovementEnabled = targetMovementEnabledField.isOn;
        config.enemeyMovementEnabled = enemyMovementEnabledField.isOn;
        config.enemyCount = int.Parse(enemyCountField.text);
        config.targetCount = int.Parse(targetCountField.text);
        config.boidCount = int.Parse(boidCountField.text);
        config.targetMaxVelocity = float.Parse(targetMaxVelocityField.text);
        config.enemyMaxVelocity = float.Parse(enemyMaxVelocityField.text);
    }

    public void ResetToDefault()
    {
        maxVelocityField.text = defaultConfig.maxVelocity.ToString(CultureInfo.InvariantCulture);
        minStartVelocityField.text = defaultConfig.minStartVelocity.ToString(CultureInfo.InvariantCulture);
        maxStartVelocityField.text = defaultConfig.maxStartVelocity.ToString(CultureInfo.InvariantCulture);
        steeringSpeedField.text = defaultConfig.steeringSpeed.ToString(CultureInfo.InvariantCulture);
        neighborRadiusField.text = defaultConfig.neighborRadius.ToString(CultureInfo.InvariantCulture);
        boidCountField.text = defaultConfig.boidCount.ToString(CultureInfo.InvariantCulture);
        targetFollowRadiusField.text = defaultConfig.targetFollowRadius.ToString(CultureInfo.InvariantCulture);
        seperationRadiusField.text = defaultConfig.seperationRadius.ToString(CultureInfo.InvariantCulture);
        fleeRadiusField.text = defaultConfig.fleeRadius.ToString(CultureInfo.InvariantCulture);
        cohesionWeightField.text = defaultConfig.weights[0].ToString(CultureInfo.InvariantCulture);
        alignmentWieghtField.text = defaultConfig.weights[1].ToString(CultureInfo.InvariantCulture);
        seperationWeightField.text = defaultConfig.weights[2].ToString(CultureInfo.InvariantCulture);
        targetWieghtField.text = defaultConfig.weights[3].ToString(CultureInfo.InvariantCulture);
        fleeWeightField.text = defaultConfig.weights[4].ToString(CultureInfo.InvariantCulture);
        cameraSpeedField.text = defaultConfig.cameraSpeed.ToString(CultureInfo.InvariantCulture);
        cameraRotationSpeedField.text = defaultConfig.rotationSpeed.ToString(CultureInfo.InvariantCulture);
        enemyCountField.text = defaultConfig.enemyCount.ToString(CultureInfo.InvariantCulture);
        targetCountField.text = defaultConfig.targetCount.ToString(CultureInfo.InvariantCulture);
        enemyMaxVelocityField.text = defaultConfig.enemyMaxVelocity.ToString(CultureInfo.InvariantCulture);
        targetMaxVelocityField.text = defaultConfig.targetMaxVelocity.ToString(CultureInfo.InvariantCulture);
        trailsEnabledField.isOn = defaultConfig.trailsEnabled;
        targetMovementEnabledField.isOn = defaultConfig.targetMovementEnabled;
        enemyMovementEnabledField.isOn = defaultConfig.enemeyMovementEnabled;

        Changed();
    }

    public void Exit()
    {
        Application.Quit();
    }
}