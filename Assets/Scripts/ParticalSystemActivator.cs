using System;
using System.Diagnostics;
using UnityEngine;

namespace DefaultNamespace
{
    public class ParticalSystemActivator : MonoBehaviour
    {
        public Configuration config;
        private ParticleSystem[] particleSystems;

        private void Awake()
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        private void Update()
        {
            foreach (ParticleSystem system in particleSystems)
            {
                if (config.trailsEnabled && !system.isPlaying)
                {
                    system.Play();
                }
                else if (!config.trailsEnabled && system.isPlaying)
                {
                    system.Stop();
                }
            }
        }
    }
}