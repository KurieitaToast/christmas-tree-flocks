﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraControls : MonoBehaviour
{
    [SerializeField] private Configuration config;
    [SerializeField] private Canvas menu;
    [SerializeField] private TMP_Text pause;
    private Controls control;

    private Vector2 move;
    private Vector2 look;
    private float up;

    private Camera cam;

    private float yaw;
    private float pitch;

    private bool freezeCameraLook;

    private void Awake()
    {
        control = new Controls();
        move = Vector2.zero;
        look = Vector2.zero;
        cam = GetComponent<Camera>();
        Cursor.visible = false;

        pause.gameObject.SetActive(config.pause);

        control.Gameplay.Move.performed += context => { move = control.Gameplay.Move.ReadValue<Vector2>(); };
        control.Gameplay.Look.performed += context => look = control.Gameplay.Look.ReadValue<Vector2>();
        control.Gameplay.Pause.performed += context =>
        {
            config.pause = !config.pause;
            Time.timeScale = config.pause ? 0 : 1;
            pause.gameObject.SetActive(config.pause);
        };
        control.Gameplay.Menu.performed += context =>
        {
            menu.gameObject.SetActive(!menu.gameObject.activeSelf);
            freezeCameraLook = !freezeCameraLook;
            Cursor.visible = !Cursor.visible;
        };

        control.Gameplay.Up.performed += context => up = control.Gameplay.Up.ReadValue<float>();
    }

    private void Update()
    {
        transform.position += Time.unscaledDeltaTime * config.cameraSpeed *
                              (transform.forward * move.y + transform.right * move.x + Vector3.up * up).normalized;


        if (!freezeCameraLook)
        {
            yaw += look.x * config.rotationSpeed * Time.unscaledDeltaTime;
            pitch -= look.y * config.rotationSpeed * Time.unscaledDeltaTime;
        }

        transform.rotation = Quaternion.Euler(pitch, yaw, 0f);
    }


    private void OnEnable()
    {
        control.Gameplay.Enable();
    }

    private void OnDisable()
    {
        control.Gameplay.Disable();
    }
}