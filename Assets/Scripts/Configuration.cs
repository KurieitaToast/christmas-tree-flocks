﻿using System.Collections;
using System.Collections.Generic;
using Behavior;
using UnityEngine;

[CreateAssetMenu]
public class Configuration : ScriptableObject
{
    public float maxVelocity = 10f;
    public float minStartVelocity = 1f;
    public float maxStartVelocity = 5f;
    public float steeringSpeed = 5f;
    public float neighborRadius = 10f;
    public float targetFollowRadius = 30f;
    public float seperationRadius = 4f;
    public float fleeRadius = 30f;
    public bool pause = false;

    public float cameraSpeed = 10f;
    public float rotationSpeed = 5f;

    public bool trailsEnabled = true;
    public bool targetMovementEnabled = false;
    public bool enemeyMovementEnabled = true;

    public int enemyCount = 1;
    public int targetCount = 1;
    public int boidCount = 400;

    public float enemyMaxVelocity = 10f;
    public float targetMaxVelocity = 21f;

    public List<BoidBehavior> behaviors;
    public List<float> weights;
}