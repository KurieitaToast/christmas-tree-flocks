﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetTextToFps : MonoBehaviour
{
    private TMP_Text text;

    private float refreshTime = 0.5f;
    private float timer;
    private int counter;
    private float lastFps;

    private void Awake()
    {
        text = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        if (timer > refreshTime)
        {
            lastFps = (float) counter / timer;
            counter = 0;
            timer = 0;
            text.text = "FPS: " + lastFps;
        }

        timer += Time.smoothDeltaTime;
        ++counter;
    }
}