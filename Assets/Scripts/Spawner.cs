﻿using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    private Collider spawnArea;
    private List<GameObject> agents;
    public Configuration config;
    public GameObject[] boids;
    public GameObject[] targets;
    public GameObject[] enemies;

    public void Awake()
    {
        spawnArea = GetComponent<Collider>();
        agents = new List<GameObject>();
        Spawn();
    }

    private Vector3 RandomPointInBounds(Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

    public void Spawn()
    {
        foreach (GameObject go in agents)
        {
            Destroy(go);
        }

        agents.Clear();

        for (int i = 0; i < config.boidCount; i++)
        {
            SpawnAgent(boids);
        }

        for (int i = 0; i < config.targetCount; i++)
        {
            SpawnAgent(targets);
        }

        for (int i = 0; i < config.enemyCount; i++)
        {
            SpawnAgent(enemies);
        }
    }

    public void SpawnAgent(GameObject[] list)
    {
        GameObject go = Instantiate(list[Random.Range(0, list.Length)], RandomPointInBounds(spawnArea.bounds),
            Quaternion.identity);
        go.GetComponent<BoundsCheck>().bounds = spawnArea.bounds;
        agents.Add(go);
    }
}