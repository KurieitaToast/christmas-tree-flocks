﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeParticalSystemColor : MonoBehaviour
{
    private ParticleSystem particle;
    public Color color;

    private void Awake()
    {
        particle = GetComponentInChildren<ParticleSystem>();
    }

    private void Update()
    {
        Gradient g = new Gradient();
        GradientColorKey[] colorKeys = new GradientColorKey[3];
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];

        Color.RGBToHSV(color, out float h, out float s, out float v);
        Color lighterColor = Color.HSVToRGB(h, s, Mathf.Max(v + 0.25f, 1f));


        alphaKeys[0] = new GradientAlphaKey(0.75f, 0f);
        alphaKeys[1] = new GradientAlphaKey(0.5f, 1f);

        colorKeys[0] = new GradientColorKey(lighterColor, 0.0f);
        colorKeys[1] = new GradientColorKey(color, 0.5f);
        colorKeys[2] = new GradientColorKey(Color.black, 0.9f);

        g.SetKeys(colorKeys, alphaKeys);

        ParticleSystem.ColorOverLifetimeModule grad = particle.colorOverLifetime;
        grad.color = g;
        grad.enabled = true;
    }
}