﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Experimental.PlayerLoop;
using Random = UnityEngine.Random;

public class TargetMovement : MonoBehaviour
{
    [SerializeField] private Configuration config;
    [SerializeField] private float speed;
    [SerializeField] private ParticleSystem[] particleSystems;
    private float r1, r2, r3;

    private void Awake()
    {
        particleSystems = GetComponentsInChildren<ParticleSystem>();

        r1 = Random.Range(1f, 3f);
        r2 = Random.Range(1f, 3f);
        r3 = Random.Range(2f, 4f);

        speed = IsTarget() ? config.targetMaxVelocity : config.enemyMaxVelocity;
    }

    private void Update()
    {
        if (IsTarget())
        {
            if (!config.targetMovementEnabled) return;
        }
        else
        {
            if (!config.enemeyMovementEnabled) return;
        }

        speed = IsTarget() ? config.targetMaxVelocity : config.enemyMaxVelocity;

        float x = Mathf.PerlinNoise(Time.time / r1, Time.time / r1) * 2 - 1;
        float y = Mathf.Sin(Time.time / r2);
        float z = Mathf.Cos(Time.time / r3);

        Vector3 dir = new Vector3(x, y, z).normalized;

        transform.position += speed * Time.deltaTime * dir;
        transform.rotation = Quaternion.LookRotation(dir);
    }

    private bool IsTarget()
    {
        return gameObject.layer == LayerMask.NameToLayer("Target");
    }
}