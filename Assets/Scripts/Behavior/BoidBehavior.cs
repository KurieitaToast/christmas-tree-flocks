﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    public abstract class BoidBehavior : ScriptableObject
    {
        private const float DEBUG_SPHERE_RADIUS = 0.25f;

        public abstract Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed);

        public virtual void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Vector3 start = agent.transform.position;
            Vector3 end = start + CalculateVelocity(agent, context, steeringSpeed);
            Gizmos.DrawLine(start, end);
            Gizmos.DrawSphere(end, DEBUG_SPHERE_RADIUS);
        }
    }
}