using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Cohesion")]
    public class CohesionBehavior : BoidBehavior
    {
        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            if (context.Count == 0) return Vector3.zero;

            Vector3 posSum = Vector3.zero;

            foreach (BoidAgent other in context)
            {
                posSum += other.transform.position;
            }

            return ((posSum / context.Count) - agent.transform.position).normalized * steeringSpeed;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Gizmos.color = Color.green;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}