using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.TerrainAPI;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Seperation")]
    public class SeperationBehavior : BoidBehavior
    {
        [SerializeField] private Configuration config;


        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Vector3 sum = Vector3.zero;
            int count = 0;

            foreach (BoidAgent other in context)
            {
                float agentsSqrDistance = Vector3.SqrMagnitude(agent.transform.position - other.transform.position);


                if (agentsSqrDistance <= config.seperationRadius * config.seperationRadius)
                {
                    sum += other.transform.position;
                    ++count;
                }
            }

            return count == 0 ? Vector3.zero : -((sum / count) - agent.transform.position).normalized * steeringSpeed;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Gizmos.color = Color.magenta;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}