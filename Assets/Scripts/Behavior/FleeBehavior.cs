using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Flee")]
    public class FleeBehavior : BoidBehavior
    {
        [SerializeField] private Configuration config;

        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Collider[] targets = Physics.OverlapSphere(agent.transform.position, config.fleeRadius,
                LayerMask.GetMask("EnemyTarget"));

            Vector3 sum = Vector3.zero;
            foreach (Collider col in targets)
            {
                sum += col.transform.position;
            }

            if (targets.Length == 0) return Vector3.zero;
            return -((sum / targets.Length) - agent.transform.position).normalized * steeringSpeed;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Gizmos.color = Color.white;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}