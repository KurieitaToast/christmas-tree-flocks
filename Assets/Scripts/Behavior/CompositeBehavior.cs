using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Composite")]
    public class CompositeBehavior : BoidBehavior
    {
        [SerializeField] private Configuration config;

        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Vector3 velocity = Vector3.zero;
            for (int i = 0; i < config.behaviors.Count; i++)
            {
                velocity += (config.behaviors[i].CalculateVelocity(agent, context, steeringSpeed)) * config.weights[i];
            }

            return velocity;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            foreach (BoidBehavior behavior in config.behaviors)
            {
                behavior.DrawDebug(agent, context, steeringSpeed);
            }

            Gizmos.color = Color.cyan;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}