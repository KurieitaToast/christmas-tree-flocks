using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Target")]
    public class TargetFollowBehavior : BoidBehavior
    {
        [SerializeField] private Configuration config;

        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Collider[] targets = Physics.OverlapSphere(agent.transform.position, config.targetFollowRadius,
                LayerMask.GetMask("Target"));
            Transform target = null;
            float sqrLastDistance = float.MaxValue;
            foreach (Collider col in targets)
            {
                if (target == null)
                {
                    target = col.transform;
                    sqrLastDistance = Vector3.SqrMagnitude(agent.transform.position - target.position);
                }
                else
                {
                    float d = Vector3.SqrMagnitude(col.transform.position - agent.transform.position);
                    if (d < sqrLastDistance)
                    {
                        target = col.transform;
                        sqrLastDistance = d;
                    }
                }
            }

            if (target == null) return Vector3.zero;
            return (target.position - agent.transform.position).normalized * steeringSpeed;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Gizmos.color = Color.white;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}