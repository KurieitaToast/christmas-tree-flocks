using System.Collections.Generic;
using UnityEngine;

namespace Behavior
{
    [CreateAssetMenu(menuName = "Behavior/Alignment")]
    public class AlignmentBehavior : BoidBehavior
    {
        public override Vector3 CalculateVelocity(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            if (context.Count == 0) return Vector3.zero;

            Vector3 velSum = Vector3.zero;

            foreach (BoidAgent other in context)
            {
                velSum += other.Velocity;
            }

            return (velSum / context.Count).normalized * steeringSpeed;
        }

        public override void DrawDebug(BoidAgent agent, List<BoidAgent> context, float steeringSpeed)
        {
            Gizmos.color = Color.yellow;
            base.DrawDebug(agent, context, steeringSpeed);
        }
    }
}