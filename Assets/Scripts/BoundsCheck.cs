﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsCheck : MonoBehaviour
{
    public Collider area;
    [HideInInspector] public Bounds bounds;


    public void Awake()
    {
        if (area != null)
        {
            bounds = area.bounds;
        }
    }

    private void Update()
    {
        Vector3 newPos = transform.position;
        bool changed = false;

        if (transform.position.x < bounds.min.x)
        {
            newPos.x = bounds.max.x;
            changed = true;
        }
        else if (transform.position.x > bounds.max.x)
        {
            newPos.x = bounds.min.x;
            changed = true;
        }

        if (transform.position.y < bounds.min.y)
        {
            newPos.y = bounds.max.y;
            changed = true;
        }
        else if (transform.position.y > bounds.max.y)
        {
            newPos.y = bounds.min.y;
            changed = true;
        }

        if (transform.position.z < bounds.min.z)
        {
            newPos.z = bounds.max.z;
            changed = true;
        }
        else if (transform.position.z > bounds.max.z)
        {
            newPos.z = bounds.min.z;
            changed = true;
        }

        if (changed)
        {
            transform.position = newPos;
        }
    }
}