﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Behavior;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class BoidAgent : MonoBehaviour
{
    [SerializeField] private BoidBehavior behavior;
    [SerializeField] private bool debug;
    [SerializeField] private Configuration config;

    private List<BoidAgent> neighbors;
    private Vector3 velocity;
    private MeshRenderer meshRenderer;
    private Color boidColor;
    private ChangeParticalSystemColor cpsc;


    public Vector3 Velocity => velocity;
    public Color BoidColor => boidColor;

    void Awake()
    {
        neighbors = new List<BoidAgent>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        cpsc = GetComponent<ChangeParticalSystemColor>();

        do
        {
            velocity = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized *
                       Random.Range(config.minStartVelocity, config.maxStartVelocity);
        } while (velocity.Equals(Vector3.zero));


        boidColor = Random.ColorHSV(0, 1);
    }

    void Update()
    {
        if (config.pause) return;

        FindNeighbors(config.neighborRadius, out neighbors);
        meshRenderer.material.color = AvgColor();
        cpsc.color = meshRenderer.material.color;

        velocity += (behavior.CalculateVelocity(this, neighbors, config.steeringSpeed)) * Time.deltaTime;

        if (velocity.sqrMagnitude > config.maxVelocity * config.maxVelocity)
        {
            velocity = velocity.normalized * config.maxVelocity;
        }

        transform.position += velocity * Time.deltaTime;
        transform.rotation = Quaternion.LookRotation(velocity, transform.up);
    }

    private void FindNeighbors(float radius, out List<BoidAgent> res)
    {
        res = new List<BoidAgent>();
        Collider[] cols = Physics.OverlapSphere(transform.position, config.neighborRadius);

        foreach (Collider col in cols)
        {
            if (col.gameObject.TryGetComponent(out BoidAgent resRb))
            {
                if (resRb == this) continue;
                res.Add(resRb);
            }
        }
    }

    private Color AvgColor()
    {
        if (neighbors.Count <= 0) return boidColor;

        Color c = boidColor;


        foreach (BoidAgent agent in neighbors)
        {
            c += agent.BoidColor;
        }

        return c / neighbors.Count;
    }

    private void OnDrawGizmos()
    {
        if (!debug) return;

        Gizmos.color = Color.red;
        if (neighbors == null) return;
        foreach (BoidAgent agent in neighbors)
        {
            Gizmos.DrawLine(transform.position, agent.transform.position);
        }


        behavior.DrawDebug(this, neighbors, config.steeringSpeed);
    }
}